### Will's Amazing Pie Recipe

#### Ingredients
* Frozen Pie Crust, 1x
* Gala Apples, 5x
* Salted Butter, 1 Stick

#### Instructions
1. Preheat oven to 300 degrees
2. Fit the frozen crust into your pie dish
3. Lay in apples, diced
4. Cover with crust lid and add slits for steam release
5. Coat lid with butter for browning
6. Bake at 300 degrees for 1 hour until browned
7. Let cool on a wire rack, approx 30 minutes

##### Make sure to like and share on Facebook, your vegetarian friends will love this one!
