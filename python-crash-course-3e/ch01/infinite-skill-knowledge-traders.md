I'd create a website where learners and teachers could make matches.

It would be unlike Khan Academy and Udemy, in that all the learning events would be at least partly live, person-to-person. Every student interacts with other students and the teacher.

A learning event *requires*: Time, Topic, and Teacher

May also require:
- Material
- Learners

Teachers can offer classes.
Learners can request classes.

A non-expert may offer to "peer lead" a class.

When requests and offers match up, teachers and learners would schedule the event.

Events would be tentative while below their 

Teachers: [ ] [ ]
Learners: [ ] [ ] [ ] [ ] [ ]