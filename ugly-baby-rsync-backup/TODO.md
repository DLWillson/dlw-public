1. Automatically tag backups for deletion

    - Use retention rules in /retentions
    - Support all the granularities offered in settings.EXAMPLE
    (
        use `date` to create date strings using the date formats
        use `create-test-dates` to create test dates, output as %s, instead
        consider dateutil.parser to automatically figure out date format
    )

2. Rethink logging

    - Collapse success log into regular log
    - Or, add a failure log?

- Fix all Bash variables to all upper case
