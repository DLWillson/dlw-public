# Notes on using QMK with the Inland MK47

[Read this online](https://gitlab.com/DLWillson/dlw-public/-/blob/master/QMK-with-Inland-MK47.md)

Get this (and a bunch of other crap)

```bash
cd ~/git/
git clone https://gitlab.com/dlwillson/dlw-public.git
cd dlw-public/
ls -l QMK-with-Inland-MK47.md
```

### Pi the things

```bash
python3 -m pip install qmk
export PATH=$PATH:/home/pi/.local/bin
qmk setup
sudo cp /home/pi/qmk_firmware/util/udev/50-qmk.rules /etc/udev/rules.d/
```

### Ubuntu the things...

Install wb32-dfu-updater

```bash
cd ~/git/
git clone https://github.com/WestberryTech/wb32-dfu-updater.git
cd wb32-dfu-updater/
sudo apt install libudev-dev cmake libusb
sudo ./bootstrap.sh install
which wb32-dfu-updater_cli
```

Install QMK

```bash
cd ~/git/
git clone https://github.com/qmk/qmk_firmware.git
cd qmk_firmware/
qmk setup
sudo cp /home/dlwillson/git/qmk_firmware/util/udev/50-qmk.rules /etc/udev/rules.d/
sudo apt install python3-pip
python3 -m pip install --user qmk
```

Configure and use QMK

```bash
echo PATH='$PATH:/home/dlwillson/.local/bin' >> ~/.bashrc
ln -s ~/git/qmk_firmware ~/
qmk setup
qmk config user.keyboard=inland/mk47
qmk config user.keymap=dlwillson0
qmk new-keymap
code /home/dlwillson/git/qmk_firmware/keyboards/inland/mk47/keymaps/dlwillson0/keymap.c 
qmk compile

# disco keyb
# hold 0,0(escape)
# connect keyb

qmk flash
```

### Debian the things (WIP)

```bash
sudo apt install apt-file
sudo apt-file update
apt-file search bin/pip
apt install python3-pip python3-venv
python3 -m venv qmk
cd qmk/
source bin/activate
pip install qmk
qmk setup
sudo cp /home/dlwillson/qmk_firmware/util/udev/50-qmk.rules /etc/udev/rules.d/
qmk setup
qmk config user.keyboard=inland/mk47
```

Install wb32-dfu-updater

```bash
cd ~/git/
git clone https://github.com/WestberryTech/wb32-dfu-updater.git
cd wb32-dfu-updater/
sudo apt install libudev-dev cmake
sudo ./bootstrap.sh install
which wb32-dfu-updater_cli
```

Visit https://config.qmk.fm. Save down a layout.

```bash
qmk flash ~/Downloads/layout.json
```