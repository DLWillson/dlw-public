#!/bin/bash -eu
# David L. Willson

# install mailutils
# configured /etc/aliases
#    root:      dlwillson
#    dlwillson: \dlwillson,dlwillson@thegeek.nu,dlwillson@thegeek.nu
# run newaliases
# test

# add /home/dlwillson/backup-essex.sh to crontab
# 45 3,21 * * * /home/dlwillson/backup-essex.sh >> log-$( date --iso ) 2>&1
# 40 3,21 * * * tail -n 100 log-$( date --iso ) > email; /usr/bin/uuencode log log.txt >> email; cat email | /usr/bin/mail -s log-$( date --iso ) dlwillson; rm email

echo "$0 : $( date ) : begin"
echo "$0 : $( date ) : cleanup from last time, if needed..."
sudo umount /mnt                                                          || echo "no problem..."
ssh -i backup-essex essex sudo exportfs -u geneva:/mnt/                   || echo "no problem..."
ssh -i backup-essex essex sudo service nfs-kernel-server stop             || echo "no problem..."
ssh -i backup-essex essex sudo umount /dev/essex-vg/machinesnap /mnt      || echo "no problem..."
ssh -i backup-essex essex sudo lvremove --force /dev/essex-vg/machinesnap || echo "no problem..."

echo "$0 : $( date ) : create, mount, and export snapshot volume..."
ssh -i backup-essex essex sudo lvcreate -n machinesnap -s /dev/essex-vg/lv_machines -l 30%FREE
ssh -i backup-essex essex sudo mount --verbose /dev/essex-vg/machinesnap /mnt
ssh -i backup-essex essex sudo service nfs-kernel-server start
ssh -i backup-essex essex sudo exportfs -o no_root_squash geneva:/mnt/

echo "$0 : $( date ) : a short nap and mount the export..."
sleep 10; sync
sudo mount essex:/mnt /mnt

echo "$0 : $( date ) : rsync the snapshot to geneva..."
sudo rsync -avH --inplace --delete /mnt/ /home/machines/

echo "$0 : $( date ) : rsync the snapshot to sussex..."
sudo rsync -avH --inplace --delete /home/machines/ sussex:/home/machines/

echo "$0 : $( date ) : unmount and remove snapshot volume..."
sleep 10; sync
sudo umount /mnt
ssh -i backup-essex essex sudo exportfs -u geneva:/mnt/                   || echo "no problem..."
ssh -i backup-essex essex sudo service nfs-kernel-server stop
ssh -i backup-essex essex sudo umount /mnt
ssh -i backup-essex essex sudo lvremove --force /dev/essex-vg/machinesnap
echo "$0 : $( date ) : finis."
