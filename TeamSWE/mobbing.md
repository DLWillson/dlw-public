# Why and How we Mob, v.0.1

This piece is about the Winds Room Sysadmin Team, but its principles can be adopted by any team with similar values.

## What is a mob?

A mob refers to a group of three or more engineers working together on the same task. As clusters are to machines and compute tasks, mobs are to humans and devops tasks.

## Why we mob

**Reduced power distance and knowledge gaps**: By working in a mob, we actively strive to minimize both hierarchical barriers and differences in technical knowledge within our team. This approach fosters a culture of equality, collaboration, and shared learning, allowing us to leverage diverse perspectives and expertise effectively.

**Fewer released defects**: When working as a mob, we have multiple sets of eyes and minds focused on the task at hand. This collective effort allows us to catch and address potential issues more effectively, resulting in a reduced number of defects being released.

**Higher release quality**: With the combined expertise and perspectives of the team, our mob approach ensures a higher standard of quality in our releases. By leveraging the diverse skills and knowledge of each team member, we can deliver a more robust and reliable service.

**More fun**: Working as a mob fosters a sense of camaraderie and shared experience. The collaborative nature of mobbing can make the work more enjoyable and engaging for everyone involved.

## How we mob

**One is the driver**: The driver takes on the role of actively executing tasks while screen sharing. They are responsible for implementing the necessary actions.

**One is the navigator**: The navigator focuses on reading and updating the script or plan. They provide guidance and direction to the driver, ensuring that the task is aligned with our objectives.

**Others**: In our case, we have an additional team member who acts as a heckler and scribe. This person injects humor into the process and records follow-up actions for future reference.

**Regular and frequent role rotation**: To ensure equal participation and shared understanding, we regularly rotate roles within the mob. This allows each team member to have a chance to drive, navigate, heckle, and scribe. By doing so, we empower our team members to be versatile and capable of taking on various responsibilities.

By dividing the work and regularly rotating roles, we create a collaborative environment where everyone has the opportunity to contribute and learn. This ensures that our team's service continues to be delivered effectively, even when one or more of us becomes unavailable.
