# Learning Ceph

Using Mastering Ceph book. It's pretty out-of-date: Ubuntu 16, Ceph Mimic.

[Shutting down, according to Suse](https://documentation.suse.com/ses/7.1/html/ses-all/admin-caasp-cluster.html)

To reverse the above safeties: ceph osd unset some-option

Hanging is an option for ceph status

RHEL's Ceph install docs target EL7

[Ceph Ansible's README recommends cephadm](https://github.com/ceph/ceph-ansible#ceph-ansible)

[Main Ceph page recommends cephadm or Rook](https://docs.ceph.com/en/latest/install/)

[Ceph with Proxmox](https://pve.proxmox.com/wiki/Deploy_Hyper-Converged_Ceph_Cluster)
