#!/bin/env python3

def name_stage_of_life(person):
    if person['age'] < 2:
        print(f"{person['name']} is a baby.")
    elif person['age'] < 4:
        print(f"{person['name']} is a toddler.")
    elif person['age'] < 13:
        print(f"{person['name']} is a kid.")
    elif person['age'] < 20:
        print(f"{person['name']} is a teenager.")
    elif person['age'] < 65:
        print(f"{person['name']} is an adult.")
    else:
        print(f"{person['name']} is an elder.")

people=[
    {'name': 'david','age': 57},
    {'name': 'eden','age': 0.5},
    {'name': 'liberty','age':18},
    {'name': 'liam', 'age': 3},
    {'name': 'not clara', 'age': 11},
    {'name': 'heather','age': 51},
    {'name': 'joe','age': 92},
]

for person in people:
    name_stage_of_life(person)
