#!/usr/bin/env python3

spacey_name = "\n\t- Kevin Spacey -\n"
print(spacey_name)
print("lstrip: ", spacey_name.lstrip())
print("rstrip: ", spacey_name.rstrip())
print("strip: ", spacey_name.strip())
