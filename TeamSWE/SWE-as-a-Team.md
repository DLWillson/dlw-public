# SWE as a Team

This is a script for one-hour hands-on class on coding as a team.

## Stick it in git

Git tracks the history of who did what and when they did it.

Git provides a place for the author to say why they did what they did.

Keeping code and docs near each other makes the docs easier to find when you need them.

Keeping code and docs near each other makes it easier to update one when you update the other.

## Write and release the code as a Team

It's expensive, but released defects, crappy code, and knowledge siloes are expensiver.

It's best to pair or mob from idea to production.

If you can't pair or mob the whole time, at least do frequent *live* code reviews.

Review live at every good stopping point:

    * strategy
    * design
    * unit of code (a dozen lines or a page)

Have someone other than the primary author merge the code so they have skin in the game.

Seek criticism, comments, contributions, questions... Avoid LGTMs!

An empty approval, an LGTM (Looks Good To Me) is a useless waste of time. The best way to make sure you get feedback or input, not just approval, is to keep your diff *small*.

## Rotate Mob Roles

Driver does all the clicking and typing.

Navigator reads any script and updates it and does routine accounting.

Scribe creates tickets, schedules follow ups, and tells corny jokes.

## Cautions and Exceptions

Do not branch Single Sources of Truth. Commit them straight to trunk and promptly either pull them into any working branches or rebase the working branches on trunk. Better yet, get SSOTs out of git. Git is for things that want to vary. SSOTs do not want to vary.

If actual practice matches docs and docs match actual practice and no code changed, a code review may not be needed. When in doubt, code review.

Don't let a coding team get too big. Three member mobs might be ideal in terms of inexpensively avoiding defects, grunky code, and knowledge siloes. Beware of N(N-1) communication links: 1:0, 2:2, 3:6, 4:12.

## Working Branches

Create a working branch when you are ready to start changing things, not earlier.

Return to trunk as often as you have complete increments of progress that trunk does not have.

## Exercises

### A Delicious Pie
