# sshuttle is the next best thing to being there

Got an ssh jump-host? Then you, my friend, have 99% of a VPN.

Do you need to get to some back-end storage, a back-end database, processing server, k8s node, file-server, router, or whatever, that isn't directly reachable from where you are? Is the service TCP (i.e. not UDP)? Do you have an ssh jump-host that can reach it? Then, you're two thirds of the way there.

I mean, sure, you *could* set up an ssh tunnel for each backend port you want to reach, but after the 10th one, you start to wonder if this is all there is to life. No, no it's not. One sshuttle connection, and you are *done*.

So, your network is shaped like this:

```
[ back-end things]
    |
    | network/s you can't reach
    |
[ jumphost ]
    |
    |
    |
[ Internet, WAN, other large network (optional) ]
    |
    |
    |
[ you ]
```

Requires:
- local sudo
- remote ssh with shell and home
- back-end service must use TCP

Install

```bash
# Debian, Ubuntu, etc...
sudo apt install sshuttle

# Fedora, CentOS, RHEL, etc...
sudo dnf install sshuttle

# Arch, Manjaro, etc...
sudo pacman -S sshuttle

# openSUSE, SUSE, etc...
sudo zypper install sshuttle
```

Launch

```bash
# in short: sshuttle -vNHr jump.domain.tld network ...
# examples
sshuttle --verbose --auto-nets --auto-hosts --remote fuga.sofree.us
sshuttle --verbose --auto-nets --auto-hosts --remote blackberry.sofree.us 192.168.73.0/24
sshuttle --verbose --auto-nets --auto-hosts --remote darter.sofree.us 192.168.73.0/24
```

Proxmox UI:
- https://pm.int.sofree.us:8006/
- https://pve.int.sofree.us:8006/
- https://pve-too.int.sofree.us:8006/

osadmin:cu3Oor6caehahbae

1. Install sshuttle
2. Connect to a jumphost:
    ```bash
    sshuttle --verbose --auto-nets --auto-hosts --remote fuga.sofree.us
    sshuttle --verbose --auto-nets --auto-hosts --remote blackberry.sofree.us 192.168.73.0/24
    sshuttle --verbose --auto-nets --auto-hosts --remote darter.sofree.us 192.168.73.0/24
    ```
3. Connect to a backend service:

  fuga:
  ```bash
  curl 172.21.0.2
  psql --host 172.21.0.3 --username osadmin
  ```

  darter/blackberry: Proxmox UI
  - https://pm.int.sofree.us:8006/
  - https://pve.int.sofree.us:8006/
  - https://pve-too.int.sofree.us:8006/

Finis

---

[this file home](https://gitlab.com/DLWillson/public/-/tree/master/BLUG-2022-06-06-sshuttle)

git this

```bash
cd ~/git/ # or cd to where-ever you keep clones
git clone https://gitlab.com/DLWillson/public.git dlwillson-public
```

---

Make osadmin just enough sudoer

```
dlwillson@... $ sshuttle --sudoers
Success, sudoers file update.
dlwillson@sfs-fuga-machine:~$ sudo sudo ls -l /etc/sudoers.d/
total 12
-r--r----- 1 root root 124 Jan 12 15:49 90-cloud-init-users
-r--r----- 1 root root 958 Feb 27  2021 README
-r--r----- 1 root root 156 Jun  7 02:41 sshuttle_auto
dlwillson@sfs-fuga-machine:~$ sudo cat /etc/sudoers.d/sshuttle_auto

Cmnd_Alias SSHUTTLE3B3 = /usr/bin/env PYTHONPATH=/usr/lib/python3/dist-packages /usr/bin/python3 /usr/bin/sshuttle *

dlwillson ALL=NOPASSWD: SSHUTTLE3B3

dlwillson@sfs-fuga-machine:~$ sudo sed -e 's/dlwillson/osadmin/' -i /etc/sudoers.d/sshuttle_auto
```
