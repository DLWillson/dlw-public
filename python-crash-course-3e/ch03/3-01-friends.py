#!/usr/bin/env python3
# 2024-01-29 David L. Willson
# no rights reserved

family=[
    'Davey',
    'Evan',
    'Heather',
    'Father Mike and Mother Willa',

    'Aunt Sue and Uncle Ray',
    'Jef',
    'Shamrock',
    'Samantha and Charlie',

    'Tracey',
    'Kelvin',
    'Trinity',
    'Liberty',

    'Jessica and Buck',
    'Shane',
    'Reanna',
    'Genevieve',
    'Liam',

    'Robyn and Hawker',
    'Clara Cherie',
    'Ivy Joy',
]

friends=[
    'Aaron',
    'Burt',
    'Dan',
    'Rich',
    'Alex',
    'Chris',
    'Troy',
    'Dave',
    'Tom and Jennifer',
    'Shawna',
    'Tim',
    'Randall'
]

friendly_coworkers=[
    'Heather'
    'Aaron',
    'Marcus',

    'Dusty',
    'Jason',
    'Eric',
    'Phil',
    'Spencer',
    'AV',
    'Will',
    'Aaron',
    'Yi',
    'Ruth',
    'John',
    'Brian',
    'Matt',
    'Matt',
]

for fm in family:
    print(f"I have a family member named {fm}.")

for f in friends:
    print(f"I have a friend named {f}.")

for fc in friendly_coworkers:
    print(f"I have a friendly coworker named {fc}.")
