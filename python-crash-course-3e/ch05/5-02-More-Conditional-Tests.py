#!/bin/env python3

import math

if "david" == 'david':
    # I predict true
    print('Yes, "david" equals "david".')
else:
    print("Well, that's darn surprising.")

if "david".lower() == 'David':
    # I predict false
    print("Yes, \"david\".lower() == 'David'.")
else:
    print("No, \"david\".lower() does not equal 'David'.")

print("Should I do one like they show? I predict True.")
print(bool("I should do one like they show."))

print('Is pi greater than 3? I predict True.')
print('math.pi > 3 is', math.pi > 3)

print('Is False and True, true? I predict False.')
print('False and True is', False and True)

print('Is False and True or True, true? I predict True.')
print('False and True or True is', False and True or True)

print("Is 1 in a list of coinidentally even floats which includes 1.0? Actually, I don't know, but I guess not.")
print("1 in [1.0,2.0,3.0] is", 1 in [1.0,2.0,3.0])

print("Can pi be found in a list which contains pi? I guess it can.")
print("math.pi in [1.0,2.0,3.0,math.pi] is", math.pi in [1.0,2.0,3.0,math.pi])

