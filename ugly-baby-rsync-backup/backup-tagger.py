#!/bin/env python3

# takes a list of dates from stdin
# outputs them with tags to stdout

# origin: https://gitlab.com/DLWillson/dlw-public/
# in the rsync_backup folder for now
# maybe someday in the ugly_baby_rsync_backup folder

# design decision
# tags:
# year start, year end, month start, month end
# week start, week end, day start, day end
# higher granularity tags are skipped
# i.e. if a backup is a 'year' anything, no further tags are applied

# read all dates from stdin
# convert to timestamps
# sort oldest to newest
# print with tags

import datetime
import fileinput
import time

TS0=time.strptime(time.ctime(0.0))

def week_of_year(t):
    return datetime.date(t.tm_year, t.tm_mon, t.tm_mday).strftime("%V")

def leppard(ldate, rdate):
    if ldate == TS0:
        # print('ldate is TS0. rdate must be the first.')
        return ['','first']
    if rdate == TS0:
        # print('rdate is TS0. ldate must be the last.')
        return ['last','']
    if ldate.tm_year < rdate.tm_year:
        # print('ldate is year end')
        # print('rdate is year start')
        return ['year end','year start']
    if ldate.tm_mon < rdate.tm_mon:
        # print('ldate is month end')
        # print('rdate is month start')
        return ['month end','month start']
    if week_of_year(ldate) < week_of_year(rdate):
        # print('ldate is week end')
        # print('rdate is week start')
        return ['week end','week start']
    if ldate.tm_yday < rdate.tm_yday:
        # print('ldate is day end')
        # print('rdate is day start')
        return ['day end','day start']
    return ['','']


intimes=[]
new_tags=[]
tags=dict()
intimestrings=dict()

for line in fileinput.input():
    intimestring=line.strip()
    time_struct=time.strptime(intimestring,'%Y-%m-%d')
    time_float=time.mktime(time_struct)
    intimes.append(time_float)
    intimestrings[time_float]=intimestring

for tf in intimes:
    tags[tf]=[]

tags[0.0]=[]

last_tf=0.0
for this_tf in sorted(intimes):
    last_ts=time.strptime(time.ctime(last_tf))
    this_ts=time.strptime(time.ctime(this_tf))
    new_tags=leppard(last_ts, this_ts)
    if type(new_tags) is list:
        tags[last_tf].append(new_tags[0])
        tags[this_tf].append(new_tags[1])
    # print(time.ctime(this_tf))
    last_tf=this_tf

this_tf=0.0
last_ts=time.strptime(time.ctime(last_tf))
this_ts=time.strptime(time.ctime(this_tf))
new_tags=leppard(last_ts, this_ts)
if type(new_tags) is list:
    tags[last_tf].append(new_tags[0])
    tags[this_tf].append(new_tags[1])

for tf in intimes:
    print(intimestrings[tf],tags[tf])
