#!/usr/bin/env python3

first_name='david'
last_name='willson'
full_name=f'{first_name} {last_name}'
print("Hello", full_name.title())

# same-ish, but with an f-string
print(f"Hello {full_name.title()}")

# and now with a newline
print(f"Hello there ... \n{full_name.title()}!")

# tabses
print("1\t2\t3\t4\t5")
