#!/bin/env python3

def shoot_an_alien(alien):
    if alien['color'] == "green":
        print(f"5 points for a {alien['color']} alien!")
    elif alien['color'] == "yellow":
        print(f"10 points for a {alien['color']} alien!")
    elif alien['color'] == "red":
        print(f"15 points for a {alien['color']} alien!")
    else:
        print(f"No points for a {alien['color']} alien!")

aliens=[
    {'color': 'red'},
    {'color': 'blue'},
    {'color': 'green'},
    {'color': 'yellow'}
    ]

for alien in aliens:
    shoot_an_alien(alien)
