#!/bin/env python3
# users=[]
users=['David','Doug','Kelvin','Stone','Terence']


if users:
    for user in users:
        print(f"Hello {user}")
else:
    print("We need some users!")
