#!/usr/bin/env python3
# David L. Willson
# no rights reserved

# Triple quote allows a multiline string, I think
# It *definitely* does somethig different than single quotes
# I don't know if ' and " are different in Python like they are in Bash
# I think they're not
famous_quote = """

The concept of the true nature of the self is a complex and philosophical topic that has been debated by scholars and thinkers for centuries. Different philosophical and religious traditions have varying perspectives on this matter. Some argue that the self has an inherent essence or true nature, while others propose that the self is a constantly evolving and changing entity.

From a psychological standpoint, the self can be seen as a combination of various factors, including our thoughts, emotions, beliefs, and experiences. It is influenced by our environment, relationships, and personal development. However, it's important to note that the understanding of the self is subjective and can vary from person to person.

If you're interested in exploring this topic further, I encourage you to delve into philosophical and psychological literature, which can provide different perspectives and theories on the nature of the self. Additionally, you may find it helpful to engage in introspection and self-reflection to gain personal insights.

"""

# I've been playing with Ecosia Chat, which is powered by OpenAI's GPT4
# I hate OpenAI, because it reneged on "Open" part
# It was supposed to be FLOSS
# Still, it's absurdly cool. I look forward to building similar things
# with all FLOSS

famous_author = "Ecosia Chat"

message=f"{famous_quote.strip()}\n\n-- {famous_author}"

print(message)