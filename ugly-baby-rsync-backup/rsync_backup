#!/bin/bash -eu
# David L. Willson 720-333-LANS(5267)

if [[ $( pgrep $( basename $0 ) | wc -l ) -gt 2 ]]; then
  cpid=$( pgrep $( basename $0 ) | sort | head -n 1 )
  echo "ERROR: It looks like you tried to start multiple copies of this." >&2
  echo "       This copy will abort and leave the older copy running."    >&2
  echo "=== begin diagnostics ==="  >&2
  ps $cpid                          >&2
  ls -l /proc/$cpid/stat            >&2
  cat /proc/$cpid/stat              >&2
  echo "=== end diagnostics ==="    >&2
  exit 1
fi

echo "$(date): $0 beginning..."

source "$( dirname "$0" )"/settings

if [[ -f ~/.ssh/rsync_backup.key ]]
then
	eval $( ssh-agent )
	ssh-add ~/.ssh/rsync_backup.key
fi

if [[ $( umask ) -gt 7 ]]
then
	umask 0007
fi

# The timestamp must be set in the main loop, NOT the source loop or sources will spray across timestamp folders if the timestamp changes while working through the set. This could lead to incomplete backup sets.

timestamp=$( date $dateformat )

# don't change this
success_log="$backups_root/success_log"

for source in $sources
do
	echo "$(date): rsync backup of $source beginning."
	dest="$backups_root/$timestamp/$source"
	detail_log="$backups_root/$timestamp/$( echo $source | tr -c '[:alnum:]@:' '_' ).log"

	[[ -d "$( dirname $dest )" ]] ||
		mkdir -p "$( dirname $dest )"

	[[ -f "$success_log" ]] ||
		touch "$success_log"

	[[ -f "$detail_log" ]] ||
		touch "$detail_log"

  # Seed new dest from last good backup, if any.
  if [[ -e $dest ]]; then
    echo "$dest exists. I'll update it."
  elif fgrep "$source" $success_log &> /dev/null; then
		last_good_dest=$( fgrep "$source" $success_log | tail -1  | cut -f 1 )
		echo "last_good_dest='$last_good_dest'"
		if [[ -e $last_good_dest ]]; then
			echo "Seeding $dest from $last_good_dest with cp -al"
			set -x
			cp -al "$last_good_dest" "$dest"
			set +x
		else
			echo "Skipping the seed because $last_good_dest doesn't exist."
		fi
	fi

	# rlt works better for SMB shares, since I really can't preserve perms/owner/group, and trying to do so breaks things
	echo -e "\n\n$( date )\n---\n" >> "$detail_log"
	set -x +e # turn on trace and tolerate errors during rsync
	rsync -avH --delete \
		--exclude-from=$backups_root/exclusions --delete-excluded \
		"$source" "$dest" >> "$detail_log" 2>&1
	set +x -e # and *only* during rsync
	rsync_exit_status=$?

	# Consider: The exit status of rsync is not as good as a real verification.
	# Also, sometimes, rsync exits non-zero when really, all that can be done, has been done
	# Also, failing to log "success" prevents seeding the next backup
	# So, we're logging rsync exits 0 and 23 is "success"
	if [[ "${rsync_exit_status:-}" -eq '0' ]] ||
		[[ "${rsync_exit_status:-}" -eq '23' ]]; then
		echo -e "$dest\trsync_exit_status=$rsync_exit_status\t$source" >> "$success_log"
	else
		echo "
    $0 ERROR: Backup of $source failed. Cleanup commands:
    rm -f $detail_log
    rm -rf $dest
		" >&2
	fi
	echo "$(date): rsync backup of $source finished."
done
( echo '==='; df -hT $backups_root; echo '===' )
echo "$(date): $0 finished."
