rsync_backup
===

This tool manages a directory of "backups" of specific local and remote directories using rsync.  It has a couple interesting features.  One is that it maintains a timestamped backup sets.  Second the timestamped backup sets are "thin" in that they utilize hard-links to avoid duplicate storage of untouched files.

It makes use of a configuration file in the same directory where the script runs to configure which local and remote directories to sync and how frequently new timestamped hierarchies are created.

Recommended Usage
---

-1. Stop. Don't use this. Use duplicity, rsnapshot, rdiff-backup, or something. Not this.

If you insist on using this, this is the recommended setup process.

0. edit rsync_backup.settings
1. run rsync_backup manually to test
2. configure keys for user running rsync_backup on any remote systems
3. configure a cron entry in /etc/cron.d/, see rsync_backup.crontab as an example

After a few runs and over a few increments of the desired granularity, you will have some timestamped backup set directories in $backup_root. In each timestamped backup set directory, you will have directories and logs for each of your $sources. Note that each new or touched file has a unique inode. Each untouched file shares an inode with a previously stored file. This is how rsync_backup avoids wasting space storing multiple copies of untouched files. It 'seeds' each timestamped backup set with hard-links to the last successful backup's files.

Example:

```bash
$ cat rsync_backup.settings
backups_root='/backups/'
sources="
bcvtool-po-02p.sys.comcast.net:testdata/
"
dateformat="+%F-%H%M" # minutely


$ tree -L 3 --inodes --noreport /backups/
/backups/
├── [10092929]  2015-12-07-1531
│   ├── [10092931]  bcvtool-po-02p.sys.comcast.net:testdata
│   │   ├── [10092932]  alwayschanges
│   │   └── [10092933]  neverchanges
│   └── [10092930]  bcvtool_po_02p_sys_comcast_net:testdata__.log
├── [10092934]  2015-12-07-1532
│   ├── [10092936]  bcvtool-po-02p.sys.comcast.net:testdata
│   │   ├── [10092937]  alwayschanges
│   │   └── [10092933]  neverchanges
│   └── [10092935]  bcvtool_po_02p_sys_comcast_net:testdata__.log
├── [10092938]  2015-12-07-1533
│   ├── [10092940]  bcvtool-po-02p.sys.comcast.net:testdata
│   │   ├── [10092937]  alwayschanges
│   │   ├── [10092933]  neverchanges
│   │   └── [10092941]  newfile
│   └── [10092939]  bcvtool_po_02p_sys_comcast_net:testdata__.log
├── [10092942]  2015-12-07-1535
│   ├── [10092944]  bcvtool-po-02p.sys.comcast.net:testdata
│   │   ├── [10092945]  alwayschanges
│   │   ├── [10092933]  neverchanges
│   │   └── [10092941]  newfile
│   └── [10092943]  bcvtool_po_02p_sys_comcast_net:testdata__.log
└── [10092549]  success_log
```

Notice that the inode of files that haven't changed, doesn't change, but the inode of new or changes files, does. Every timestamp-named directory looks like a full backup and contains all the files that the source contained at the time of the backup. No backup depends on other backups, in any way, so you may safely delete any timestamp when you no longer need its unique information.

For example, if the last backup of last year succeeded, you might delete all the other backups from last year. Or, if you no longer care how the data changed last week, but only how it ended, you might delete all the backups from Monday through Thursday, leaving only the backup that occured on Friday.


Checking Health
---

0. Pay attention to the stdout and especially stderr.
   If you're running rsync_backup from cron, make sure stdout and especially stderr are going somewhere you can see and review: email, logfile, or where-ever.

1. Check the date on $backups_root/success_log. Review recent entries.
   Example: ls -l /backups/success_log

2. Check the head and especially the tails of the detail logs of most recent run.
   Example: tail /backups/2015-12-31/*.log


To Do:
---

* Stop working on this. Use something else.

* write a check script to replace or augment the above "Checking Health" section.

* It should show backups as 'in-progress' so that downstream systems can sort completed backups from incomplete.

   The success_log could be changed to a summary_log, where a particular source is 'begun' then 'failed' or 'succeeded' with time-stamps. grep for 'succeeded' to get the old thing

* It should automatically name year-end backups and month-end backups, like this:

    Sort the directory entries and go through one at a time.
    IF the current entry is a different year than the last entry and the last entry's year doesn't have a yyyy-YE, then rename the last entry to yyyy-YE.
    ELIF the current entry is a different month than the last, and there is no ME for the last entry's month, rename the last entry yyyy-mm-ME.

* It should automatically check free space and clean up before trying to run any particular job.

    While free_space_of_backup_root is less than 20%
      While there are more than 7 "regular" backups, delete the oldest.
      While there are more than 3 "*-ME" backups, delete the oldest.
      While there are more than 1 "*-YE" backups, delete the oldest.
      Send an alert.


Considerations:
---

Perhaps only rsync_backup needs to write to backups_root? Perhaps that write needs to be append-only? What's the best way to solve that? Create or us a layered file-system with LVM or zfs? Separate read-only and read-write mounts? How about just 2750 permissions all through? Immutability?

- Stop sourcing settings file.  look for values in settings file
- UPPERCASE all variables MFer
- Automatically remove dest on backup failures
- Alert sysadmin upon any failures
- Installer should not install source directory, it should install specific files
- Find a FLOSS solution so we don't have to maintain this
