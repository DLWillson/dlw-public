#!/usr/bin/env python3

my_favorite_number=3

print("My favorite number is", my_favorite_number)

my_favorite_number=[7,3,1]

for n in my_favorite_number:
    print("My favorite number is", n)

other_numbers_i_like=[2,4,5,6,8,9,12,16]

print("I also like some other numbers:",other_numbers_i_like)