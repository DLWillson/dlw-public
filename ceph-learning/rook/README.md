I am leaving off here...

I think I have Rook running correctly on the cluster.

```bash
. rc
vagrant status
vagrant ssh kube1
```

```bash
ls -l
vim.tiny nginx.yaml
kubectl delete -f nginx.yaml 
kubectl create -f nginx.yaml
```

```bash
kubectl -n rook-ceph exec -it rook-ceph-tools-84f9854d5f-7lw2f -- bash
```

```bash
ceph status
ceph osd lspools
```

I'm unable to figure out how to connect the nginx pod to the cephfs.


See also:
- [installs](installs)
- [cephfs mount example](https://github.com/kubernetes/examples/blob/master/volumes/cephfs/cephfs.yaml)
- [Ceph CSI](https://github.com/ceph/ceph-csi/blob/devel/examples/cephfs/pod-clone.yaml)