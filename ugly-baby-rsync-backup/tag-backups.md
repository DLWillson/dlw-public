# backup tagger takes one or two timestamps, prev_timestamp and next_timestamp
# - next_timestamp with no prev_timestamp
# - prev_timestamp and next_timestamp
# - prev_timestamp with no next_timestamp

# it tags each timestamp, according to the following rules:

# if the month differs between the timestamps:
# prev_timestamp is tagged end_of_month_XX and next_timestamp is tagged start_of_month_YY

# if the week differs between the timestamps:
# prev_timestamp is tagged end_of_week_XX and next_timestamp is tagged start_of_week_YY

# if the day differs between the timestamps:
# prev_timestamp is tagged end_of_day_XX and next_timestamp is tagged start_of_day_YY

# it returns one or two arrays of the form:
# { timestamp, tags, ...}

# if next_timestamp is null, prev_timestamp

Backup tagger works with unsorted dates!

Have create-test-dates instead create test backup directories with touch. Then, all other utilities can act on that, instead.
