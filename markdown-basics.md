Markdown Basics
===

see also:
- [Origin](
  https://daringfireball.net/projects/markdown/)
- [GitHub Flavored Markdown](
  https://help.github.com/categories/writing-on-github/)
- [GitLab Flavored Markdown](
  https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/markdown/markdown.md)

Markdown is a set of text-formatting conventions that look good in a text viewer/editor *and* (rendered as HTML) in a web browser.

See this file in atom (or your favorite text-editor):

```shell
cd ~/git/spectra-ops && git pull
# or:
# mkdir -p ~/git &&
# cd ~/git/ &&
# git clone git@github.comcast.com:networkoss/spectra-ops.git
# cd spectra-ops
cd docs/markdown-basics
atom README.md
# or:
# vim README.md
# or:
# gedit README.md
```

See [this file in github](https://github.comcast.com/networkoss/spectra-ops/tree/master/docs/markdown-basics)

The very basics:
- Headers with hashes and dashes
- *asterisks* *italicize*...
- **double-asterisks** **bold**
- [link](www.link.com)
- code snippets

  ```shell
  for i in $( seq 10 ); do echo $i; done
  ```

  ```javascript
  var s = "JavaScript syntax highlighting";
  alert(s);
  ```

  ```python
  s = "Python syntax highlighting"
  print s
  ```

  ```
  No language indicated, so no syntax highlighting.
  But let's throw in a <b>tag</b>.
  ```

- bullet-lists with dash
  * asterisk
    + plus
  * asterisk
    + plus
- and nested bullets
- work fine

1. cascaded number-lists
2. work somtimes.
  1. this
  2. that
  3. the other thing
3. See?

Now, you try it.

[Markdown Tutorial](http://www.markdowntutorial.com/)

---

- partial credit to:
  https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
