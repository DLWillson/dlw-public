import random

def print_list_with_abbreviated_list(somelist):
    print("whole list")
    print("----------")
    print(somelist,'\n')

    head_of_list=somelist[:3]
    
    if len(somelist)%2:
        # length of list is odd, so there is an element in the middle
        middle_of_list=somelist[int(len(somelist)/2)-1:int(len(somelist)/2)+2]
    else:
        # length of list is even, so there are two elements nearest the middle
        middle_of_list=somelist[int(len(somelist)/2)-2:int(len(somelist)/2)+2]
    
    tail_of_list=somelist[-3:]
    
    abbreviated_list=head_of_list + ['...'] + middle_of_list + ['...'] + tail_of_list
    
    print("abbreviated list")
    print("----------------")
    print(abbreviated_list,'\n')


mutable_list=list(range(256))
random.shuffle(mutable_list)
shuffled_list=mutable_list
del mutable_list
print_list_with_abbreviated_list(shuffled_list)
print_list_with_abbreviated_list(list(range(10,25)))
print_list_with_abbreviated_list(list(range(16)))