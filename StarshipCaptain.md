# My Starship Captain list, plus

## Top 3

1. [Homeworlds](https://www.looneylabs.com/content/homeworlds) (binary and others)
2. [Alien City](https://www.ludism.org/ppwiki/AlienCity)
3. [Pikemen](http://brightestbulb.net/games/pikemen/)

## Plus 4 more

* Zendo - the old one [Kory Heath](http://www.koryheath.com/zendo/) [Looney Labs](https://www.looneylabs.com/content/zendo)
* [Zark City](https://www.looneylabs.com/content/zark-city)
* Sprawl [Invisible City](http://invisible-city.com/content/sprawl) [SuperDuperGames](http://superdupergames.org/gameinfo.html?game=sprawl)
* [Icehouse](https://www.looneylabs.com/rules/icehouse)

## Plus 3 for the Academy

* Black ICE
* RAMbots
* Gnaqush

## plus these cherished favorites

* Penguin Soccer
* Treehouse
* Zagami
* World War 5
* Martian Coasters
* Martian Chess
* Ice Dice
* CrackeD Ice / EvanIce
* Volcano or Caldera

## Pyramao

Finally, a mention of my perpetually incomplete pet project. I want to create a version of Mao for pyramids. Here is the un-tested idea stub for [Pyramao](https://gitlab.com/LPGD/pyramao).
